package Calculator;


import org.apache.log4j.Logger;

import java.util.Stack;


public class NumberStack {
  static final Logger log = Logger.getLogger(NumberStack.class);
  static final int ZERO = 0;
  private Stack<Double> numbers;
  private int Cnt;


  public NumberStack(){
    log.debug("Init of number stack");
    numbers = new Stack<Double>();
    Cnt = ZERO;
  }


  public void push(Double number){
    log.debug("Push number: " + number);
    numbers.push(number);
    Cnt++;
  };


  public Double pop(){
    Cnt--;
    return numbers.pop();
  };


  public Double peekLast(){
    return  numbers.peek();
  }

  public int getCountOfElements() {
    return Cnt;
  }
}