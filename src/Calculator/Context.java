package Calculator;

import java.util.HashMap;
import java.util.Map;

public class Context {
  public final NumberStack stack;
  public final Map<String,Double> variables;

  public Context(){
    stack = new NumberStack();
    variables = new HashMap<>();
  }

}
