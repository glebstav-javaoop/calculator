package Calculator;

import org.apache.log4j.Logger;

import java.io.*;

public class InstructionReader {
  static final Logger log = Logger.getLogger(InstructionReader.class);
  static final String L_DEB_INIT_READ = "Start of init reader";
  static final String L_DEB_READ_CONSOLE = "Attempt to read from console";
  static final String L_DEB_READ_FILE = "Attempt to read from file";
  static final String L_DEB_READ_LINE = "Attempt to read line";
  static final String L_DEB_SPLIT_LINE = "Attempt to split line ";
  static final String L_INF_INIT = "The reader was successfully initialized";
  static final String L_WAR_READ = "Can't get line from reader";
  static final String SPLIT_DIVIDER = " ";
  static final String QUOTE = "'";
  static final String E_FILE = "File ";
  static final String E_FILE_NOT_FOUND = " not found";
  static final String E_READING = "Reading from the console";
  static final Integer FILENAME_ARGUMENT = 0;
  static final Integer EMPTY_LENGTH_ARGUMENT = 0;
  private BufferedReader reader;

  public InstructionReader(String[] args) {
    log.debug(L_DEB_INIT_READ);
    if (args.length == EMPTY_LENGTH_ARGUMENT) {
      log.debug(L_DEB_READ_CONSOLE);
      reader = new BufferedReader(new InputStreamReader(System.in));
    } else {
      try {
        log.debug(L_DEB_READ_FILE);
        reader = new BufferedReader(new FileReader(args[FILENAME_ARGUMENT]));
      } catch (FileNotFoundException e) {
        log.warn(E_FILE + args[FILENAME_ARGUMENT] + E_FILE_NOT_FOUND);
        System.out.println(E_FILE + args[FILENAME_ARGUMENT] + E_FILE_NOT_FOUND);
        System.out.println(E_READING);
        log.warn(E_READING);
        log.debug(L_DEB_READ_CONSOLE);
        reader = new BufferedReader(new InputStreamReader(System.in));
      }
    }
    log.info(L_INF_INIT);
  }

  public String readLine() {
    try {
      log.debug(L_DEB_READ_LINE);
      return reader.readLine();
    } catch (IOException e) {
      log.warn(L_WAR_READ, e);
      e.printStackTrace();
      System.out.println(e.getClass());
      return null;
    }
  }

  public String[] getSplitLine() {
    String argumentsLine;
    if ((argumentsLine = readLine()) == null) {
      return null;
    }
    log.debug(L_DEB_SPLIT_LINE + QUOTE + argumentsLine + QUOTE);
    return argumentsLine.split(SPLIT_DIVIDER);
  }

}
