package Calculator.Exception;

public class DivisionByZeroException extends ArithmeticException {
  static final String TO_STRING_PREFIX = "DivisionByZeroException{Attempt to divide: ";
  static final String TO_STRING_POSTFIX = " by zero}";
  Double num;

  public DivisionByZeroException(Double num) {
    super();
    this.num = num;
  }

  @Override
  public String toString() {
    return TO_STRING_PREFIX + num + TO_STRING_POSTFIX;
  }
}
