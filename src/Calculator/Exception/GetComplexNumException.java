package Calculator.Exception;

public class GetComplexNumException extends ArithmeticException {
  static final String TO_STRING_PREFIX = "GetComplexNumException{Trying to get complex num from: ";
  static final String TO_STRING_POSTFIX = " }";
  Double num;

  public GetComplexNumException(Double num) {
    super();
    this.num = num;
  }


  @Override
  public String toString() {
    return TO_STRING_PREFIX + num + TO_STRING_POSTFIX;
  }
}
