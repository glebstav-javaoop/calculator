package Calculator.Exception;

import java.util.NoSuchElementException;

public class OperationDoesNotExistException extends NoSuchElementException {
  static final String TO_STRING_PREFIX = "OperationDoesNotExistException{wrongOperation='";
  static final String TO_STRING_POSTFIX = "' }";
  String wrongOperation;

  public OperationDoesNotExistException(String wrongOperation) {
    super();
    this.wrongOperation = wrongOperation;
  }

  @Override
  public String toString() {
    return TO_STRING_PREFIX + wrongOperation + TO_STRING_POSTFIX;
  }
}
