package Calculator.Exception;

public class WrongCountOfArgumentsException extends RuntimeException {
  static final String TO_STRING_PREFIX = "WrongCountOfArgumentsException{needArgs=";
  static final String TO_STRING_ARG1 = ", haveArgs=";
  static final String TO_STRING_POSTFIX = "}";

  int needArgs, haveArgs;

  public WrongCountOfArgumentsException(int needArgs, int haveArgs) {
    super();
    this.haveArgs = haveArgs;
    this.needArgs = needArgs;
  }

  @Override
  public String toString() {
    return TO_STRING_PREFIX + needArgs +
        TO_STRING_ARG1 + haveArgs +
        TO_STRING_POSTFIX;
  }
}
