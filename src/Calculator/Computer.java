package Calculator;
import org.apache.log4j.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import Calculator.Exception.*;
import Calculator.Operation.*;

public final class Computer {
  static final int CLASSNAME_ARGUMENT = 0;
  static final int OPERATION_ARGUMENT = 1;
  static final Logger log = Logger.getLogger(Computer.class);
  static final String L_DEB_START_INIT = "Start of init computer";
  static final String L_DEB_START_EXEC_COMP = "Start of executing computer";
  static final String L_DEB_START_OP_INIT = "Start of init operation: '";
  static final String L_DEB_START_CONF = "Start of loading configuration";
  static final String L_DEB_START_EXEC_OP = "Start of exec operation";
  static final String L_DEB_END_CONF = "Configurations loaded";
  static final String L_INF_END_INIT = "The computer was successfully initialized";
  static final String L_INF_END_OF_EXEC = "Operations were completed";
  static final String L_FAT_ERR = "Fatal error at operation: '";
  static final String L_INF_OP = "Operation '";
  static final String L_INF_COMPETE = "' completed successfully";
  static final String E_INIT = "At initialization operation does'nt exist: ";
  static final String E_EXEC = "For instruction at execution: ";
  static final String E_VAR_EXEC = "Variable isn't set at execution: ";
  static final String E_BAD_CONFIG = "Bad configuration file: ";
  static final String E_OPEN = "Can't open ";
  static final String E_READ = "Can't read ";
  static final String FILENAME_CONFIG = "configuration";
  static final String OPERATION_CLASS = Operation.class.getPackageName();
  static final String CLASS_DIVIDER = ".";
  static final String WORD_DIVIDER = " ";
  static final String QUOTE = "'";
  static final Character ENDL = '\n';
  static final Class<?>[] OPERATION_CONSTRUCTOR_PARAMETER_TYPES = {Context.class, String[].class};
  private final InstructionReader reader;
  private final Map<String, String> classNames;     //  <Operation in instruction , Name of class>
  private final Context context;


  public Computer(InstructionReader reader) throws IOException {
    log.debug(L_DEB_START_INIT);
    this.reader = reader;
    classNames = new HashMap<>();
    context = new Context();
    loadConfig();
    log.info(L_INF_END_INIT);
  }


  public void execute() throws NoSuchMethodException {
    String className;
    String[] arguments;
    log.debug(L_DEB_START_EXEC_COMP);
    while ((arguments = reader.getSplitLine()) != null) {
      try {
        className = classNames.get(arguments[CLASSNAME_ARGUMENT]);
        log.debug(L_DEB_START_OP_INIT + String.join(WORD_DIVIDER, arguments) + QUOTE);
        Operation operation = (Operation) Class.forName(OPERATION_CLASS + CLASS_DIVIDER + className)
            .getConstructor(OPERATION_CONSTRUCTOR_PARAMETER_TYPES).newInstance(context, arguments);
        log.debug(L_DEB_START_EXEC_OP);
        operation.execute();
        log.info(L_INF_OP + String.join(WORD_DIVIDER, arguments) + L_INF_COMPETE);
      } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | InvocationTargetException |
          WrongCountOfArgumentsException | OperationDoesNotExistException exception) {
        log.error(E_INIT + QUOTE + arguments[CLASSNAME_ARGUMENT] + QUOTE, exception.getCause());
        System.err.println(E_INIT + QUOTE + arguments[CLASSNAME_ARGUMENT] + QUOTE + ENDL + exception.getCause());
      } catch (GetComplexNumException | NotEnoughElementsInStackException |
          DivisionByZeroException exception) {
        log.error(E_EXEC + String.join(WORD_DIVIDER, arguments), exception);
        System.err.println(E_EXEC + String.join(WORD_DIVIDER, arguments) + ENDL + exception);
      } catch (NumberFormatException exception) {
        log.error(E_VAR_EXEC + String.join(WORD_DIVIDER, arguments), exception);
        System.err.println(E_VAR_EXEC + String.join(WORD_DIVIDER, arguments) + ENDL + exception);
      } catch (NoSuchMethodException exception) {
        log.fatal(E_BAD_CONFIG + FILENAME_CONFIG, exception);
        System.err.println(E_BAD_CONFIG + FILENAME_CONFIG + ENDL + exception);
        throw exception;
      } catch (Exception exception) {
        log.fatal(L_FAT_ERR + String.join(WORD_DIVIDER, arguments) + QUOTE, exception);
        throw exception;
      }
    }
    log.info(L_INF_END_OF_EXEC);
  }


  private void loadConfig() throws IOException {
    log.debug(L_DEB_START_CONF);
    BufferedReader configReader = null;
    try {
      configReader = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(FILENAME_CONFIG)));
    } catch (Exception exc) {
      log.fatal(E_OPEN + QUOTE + FILENAME_CONFIG + QUOTE + WORD_DIVIDER, exc);
      System.err.println(E_OPEN + QUOTE + FILENAME_CONFIG + QUOTE + WORD_DIVIDER + exc);
      throw exc;
    }
    String config, className, operation;
    try {
      while ((config = configReader.readLine()) != null) {
        className = (config.split(WORD_DIVIDER))[CLASSNAME_ARGUMENT];
        operation = (config.split(WORD_DIVIDER))[OPERATION_ARGUMENT];
        classNames.put(operation, className);
      }
    } catch (Exception exc) {
      log.fatal(E_READ + QUOTE + FILENAME_CONFIG + QUOTE + WORD_DIVIDER, exc);
      System.err.println(E_READ + QUOTE + FILENAME_CONFIG + QUOTE + WORD_DIVIDER + exc);
      throw exc;
    }
    log.debug(L_DEB_END_CONF);
  }
}

