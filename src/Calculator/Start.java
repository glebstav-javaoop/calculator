package Calculator;


import java.io.IOException;


public class Start {
  static public void main(String[] args) throws IOException, NoSuchMethodException {
    InstructionReader instructionReader = new InstructionReader(args);
    Computer computer = new Computer(instructionReader);
    computer.execute();
  }

}
