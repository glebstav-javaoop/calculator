package Calculator.Operation;
import Calculator.Context;
import Calculator.Exception.GetComplexNumException;
import Calculator.Exception.NotEnoughElementsInStackException;
import org.apache.log4j.Logger;

public class SquareRoot extends Operation {
  static final Logger log = Logger.getLogger(SquareRoot.class);
  static final String L_ER_EL_IN_STACK = "Not enough element in stack";
  static final String L_ER_SQRT_NEGATIVE = "Get negative number in SquareRoot";
  static final String L_TR_ATTEMPT_POP = "Attempt to pop 1 num";
  static final String L_TR_SQRT_COMPETE = "SquareRoot compete successfully";
  static final int CNT_OF_ARG_IN_STACK_FOR_SQUAREROOT = 1;
  static final int CNT_OF_ARG_FOR_SQUAREROOT = 1;
  static final double PERMITTED_LIMIT_FOR_SQUAREROOT = 0.0;
  public SquareRoot(Context context, String[] arguments) {
    super(context, arguments,CNT_OF_ARG_FOR_SQUAREROOT);
  }

  @Override
  public void execute() {
    if(stack.getCountOfElements()>= CNT_OF_ARG_IN_STACK_FOR_SQUAREROOT) {
      log.trace(L_TR_ATTEMPT_POP);
      Double a = stack.pop();
      if (a >= PERMITTED_LIMIT_FOR_SQUAREROOT) {
        a = Math.sqrt(a);
        stack.push(a);
        log.trace(L_TR_SQRT_COMPETE);
      } else{
        stack.push(a);
        log.error(L_ER_SQRT_NEGATIVE);
        throw new GetComplexNumException(a);
      }
    } else{
      log.error(L_ER_EL_IN_STACK);
      throw new NotEnoughElementsInStackException();
    }


  }
}
