package Calculator.Operation;

import Calculator.Context;
import Calculator.Exception.NotEnoughElementsInStackException;
import org.apache.log4j.Logger;

public class Pop extends Operation {
  static final Logger log = Logger.getLogger(Pop.class);
  static final String L_ER_EL_IN_STACK = "Not enough element in stack";
  static final String L_TR_POP_COMPETE = "Pop complete successfully";
  static final int MIN_ELEMENTS_IN_STACK_FOR_POP = 1;
  static final int CNT_OF_ARG_FOR_POP = 1;

  public Pop(Context context, String[] arguments) {
    super(context, arguments,CNT_OF_ARG_FOR_POP);
  }

  @Override
  public void execute() {
    if (stack.getCountOfElements() >= MIN_ELEMENTS_IN_STACK_FOR_POP) {
      stack.pop();
      log.debug(L_TR_POP_COMPETE);
    }
    else{
      log.error(L_ER_EL_IN_STACK);
      throw new NotEnoughElementsInStackException();
    }
  }
}
