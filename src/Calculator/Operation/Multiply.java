package Calculator.Operation;

import Calculator.Context;
import Calculator.Exception.NotEnoughElementsInStackException;
import org.apache.log4j.Logger;

public class Multiply extends Operation {
  static final Logger log = Logger.getLogger(Multiply.class);
  static final String L_TR_ATTEMPT_POP = "Attempt to pop 2 num";
  static final String L_TR_MUL_SUCCESS = "Multiplying complete successfully";
  static final String L_ER_EL_IN_STACK = "Not enough element in stack";
  static final int CNT_OF_ELEMENTS_IN_STACK_FOR_MUL = 2;
  static final int CNT_OF_ARG_FOR_MUL = 1;

  public Multiply(Context context, String[] arguments) {
    super(context, arguments, CNT_OF_ARG_FOR_MUL);
  }

  @Override
  public void execute() {
    if (stack.getCountOfElements() >= CNT_OF_ELEMENTS_IN_STACK_FOR_MUL) {
      log.trace(L_TR_ATTEMPT_POP);
      Double a = stack.pop();
      Double b = stack.pop();
      stack.push(a * b);
      log.trace(L_TR_MUL_SUCCESS);
    } else{
      log.error(L_ER_EL_IN_STACK);
      throw new NotEnoughElementsInStackException();
    }
  }
}
