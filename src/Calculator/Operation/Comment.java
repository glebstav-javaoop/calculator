package Calculator.Operation;

import Calculator.Context;
import org.apache.log4j.Logger;

public class Comment extends Operation {
  static final Logger log = Logger.getLogger(Comment.class);
  static final String L_TR_PRINT = "Print comments";
  static final String DIVIDER_OF_WORDS = " ";
  static final String END_L = "\n";

  public Comment(Context context, String[] arguments) {
    this.arguments = arguments;
  }

  @Override
  public void execute() {
    log.trace(L_TR_PRINT);
    for (String comment : arguments)
      System.out.print(comment + DIVIDER_OF_WORDS);
    System.out.print(END_L);
  }
}
