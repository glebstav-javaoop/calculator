package Calculator.Operation;

import java.io.IOException;

public interface Executable {
  public void execute();
}
