package Calculator.Operation;

import Calculator.Context;
import Calculator.Exception.WrongCountOfArgumentsException;
import Calculator.NumberStack;
import org.apache.log4j.Logger;


import java.util.Map;

abstract public class Operation implements Executable {
  static final Logger log = Logger.getLogger(Operation.class);
  static final String L_ER_CNT_OF_ARGS = "Wrong count of arguments";
  static final String L_TR_INIT = "Init of operation";
  static final String L_TR_COMPLETE_INIT = "Init compete successfully";
  protected String[] arguments;
  protected NumberStack stack;
  protected Map<String,Double> variables;

  public Operation(Context context, String[] arguments, int CNT_OF_ARGS_FOR_OPERATION) {
    log.trace(L_TR_INIT);
    if(arguments.length != CNT_OF_ARGS_FOR_OPERATION){
      log.error(L_ER_CNT_OF_ARGS);
      throw new WrongCountOfArgumentsException(CNT_OF_ARGS_FOR_OPERATION,arguments.length);
    }
    this.arguments = arguments;
    stack = context.stack;
    variables = context.variables;
    log.trace(L_TR_COMPLETE_INIT);
  }

  protected Operation() {
  }
}
