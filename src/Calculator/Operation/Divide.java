package Calculator.Operation;

import Calculator.Context;
import Calculator.Exception.DivisionByZeroException;
import Calculator.Exception.NotEnoughElementsInStackException;
import org.apache.log4j.Logger;

public class Divide extends Operation {
  static final Logger log = Logger.getLogger(Divide.class);
  static final String L_TR_ATTEMPT_POP = "Attempt to pop 2 num";
  static final String L_TR_DIV_SUCCESS = "Division complete successfully";
  static final String L_ER_DIV_BY_ZERO = "Trying to divide by zero";
  static final String L_ER_EL_IN_STACK = "Not enough element in stack";
  static final Double ZERO = 0.0;
  static final int CNT_OF_ELEMENTS_IN_STACK_FOR_DIVIDE = 2;
  static final int CNT_OF_ARG_FOR_DIVIDE = 1;

  public Divide(Context context, String[] arguments) {
    super(context, arguments, CNT_OF_ARG_FOR_DIVIDE);
  }

  @Override
  public void execute() {
    Double a, b;
    if (stack.getCountOfElements() >= CNT_OF_ELEMENTS_IN_STACK_FOR_DIVIDE) {
      log.trace(L_TR_ATTEMPT_POP);
      a = stack.pop();
      b = stack.pop();
      if (!b.equals(ZERO)){
        stack.push(a / b);
        log.trace(L_TR_DIV_SUCCESS);
      }
      else {
        stack.push(b);
        stack.push(a);
        log.error(L_ER_DIV_BY_ZERO);
        throw new DivisionByZeroException(a);

      }
    } else{
      log.error(L_ER_EL_IN_STACK);
      throw new NotEnoughElementsInStackException();
    }
  }
}
