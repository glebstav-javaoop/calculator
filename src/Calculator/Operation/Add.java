package Calculator.Operation;

import Calculator.Context;
import Calculator.Exception.NotEnoughElementsInStackException;
import org.apache.log4j.Logger;

public class Add extends Operation {
  static final Logger log = Logger.getLogger(Add.class);
  static final String L_TR_ATTEMPT_POP = "Attempt to pop 2 numbers";
  static final String L_TR_ADD_SUCCESS = "Addition compete successfully";
  static final String L_ER_EL_IN_STACK = "Not enough element in stack";
  static final int CNT_OF_ELEMENTS_IN_STACK_FOR_ADD = 2;
  static final int CNT_OF_ARG_FOR_ADD = 1;

  public Add(Context context, String[] arguments) {
    super(context, arguments, CNT_OF_ARG_FOR_ADD);
  }

  @Override
  public void execute() {
    if (stack.getCountOfElements() >= CNT_OF_ELEMENTS_IN_STACK_FOR_ADD) {
      log.trace(L_TR_ATTEMPT_POP);
      Double a = stack.pop();
      Double b = stack.pop();
      stack.push(a + b);
      log.trace(L_TR_ADD_SUCCESS);
    } else{
      log.error(L_ER_EL_IN_STACK);
      throw new NotEnoughElementsInStackException();
    }
  }
}
