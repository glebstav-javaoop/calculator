package Calculator.Operation;

import Calculator.Context;
import org.apache.log4j.Logger;

public class Push extends Operation{
  static final Logger log = Logger.getLogger(Push.class);
  static final String L_TR_PUSH_COMPLETE = "Push compete successfully";
  static final int NUMBER_ARGUMENT = 1;
  static final int CNT_OF_ARG_FOR_PUSH = 2;

  public Push(Context context,String[] arguments){
    super(context, arguments,CNT_OF_ARG_FOR_PUSH);
  }

  @Override
  public void execute() {
    if(variables.containsKey(arguments[NUMBER_ARGUMENT])){
      stack.push(variables.get(arguments[NUMBER_ARGUMENT]));
      log.trace(L_TR_PUSH_COMPLETE);
    } else {
        stack.push(Double.parseDouble(arguments[NUMBER_ARGUMENT]));
    }
  }
}
