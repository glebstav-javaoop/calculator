package Calculator.Operation;

import Calculator.Context;
import Calculator.Exception.NotEnoughElementsInStackException;
import org.apache.log4j.Logger;


public class Subtraction extends Operation{;
  static final Logger log = Logger.getLogger(Subtraction.class);
  static final String L_ER_EL_IN_STACK = "Not enough element in stack";
  static final String L_TR_ATTEMPT_POP = "Attempt to pop 2 numbers";
  static final String L_TR_SUB_COMPETE = "Subtraction compete successfully";
  static final int CNT_OF_ELEMENTS_IN_STACK_FOR_SUB = 2;
  static final int CNT_OF_ARG_FOR_SUB = 1;
  public Subtraction(Context context, String[] arguments) {
    super(context, arguments,CNT_OF_ARG_FOR_SUB);
  }

  @Override
  public void execute() {
    if(stack.getCountOfElements()>= CNT_OF_ELEMENTS_IN_STACK_FOR_SUB){
      log.debug(L_TR_ATTEMPT_POP);
      Double a = stack.pop();
      Double b = stack.pop();
      stack.push(a-b);
      log.debug(L_TR_SUB_COMPETE);
    } else{
      log.error(L_ER_EL_IN_STACK);
      throw new NotEnoughElementsInStackException();
    }
  }
}
