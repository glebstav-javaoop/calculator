package Calculator.Operation;

import Calculator.Context;
import Calculator.Exception.NotEnoughElementsInStackException;
import org.apache.log4j.Logger;

public class Print extends Operation {
  static final Logger log = Logger.getLogger(Print.class);
  static final String L_ER_EL_IN_STACK = "Not enough element in stack";
  static final String L_TR_PRINT_COMPLETE = "Print compete successfully";
  static final int MIN_ELEMENTS_IN_STACK_FOR_PRINT = 1;
  static final int CNT_OF_ARG_FOR_PRINT = 1;

  public Print(Context context, String[] arguments){
    super(context, arguments,CNT_OF_ARG_FOR_PRINT);
  }

  @Override
  public void execute() {
    if(stack.getCountOfElements()>= MIN_ELEMENTS_IN_STACK_FOR_PRINT) {
      System.out.println(stack.peekLast());
      log.trace(L_TR_PRINT_COMPLETE);
    } else{
      log.error(L_ER_EL_IN_STACK);
      throw new NotEnoughElementsInStackException();
    }
  }
}
