package Calculator.Operation;

import Calculator.Context;
import org.apache.log4j.Logger;

public class SetVariable extends Operation {
  static final Logger log = Logger.getLogger(SetVariable.class);
  static final String L_TR_PARSE = "Num parsed successfully";
  static final String L_TR_VAR = "Variable '";
  static final String L_TR_VAR_FOUND = "Variable was found";
  static final String L_TR_SET_COMPETE = "' set successfully";
  static final int VARIABLE_ARGUMENT = 1;
  static final int VALUE_ARGUMENT = 2;
  static final int CNT_OF_ARG_FOR_SETVARIABLE = 3;

  public SetVariable(Context context, String[] arguments) {
    super(context, arguments, CNT_OF_ARG_FOR_SETVARIABLE);
  }

  @Override
  public void execute() {
    String var = arguments[VARIABLE_ARGUMENT];
    Double value;
    if(variables.containsKey(arguments[VALUE_ARGUMENT])){
      log.trace(L_TR_VAR_FOUND);
      value = variables.get(arguments[VALUE_ARGUMENT]);
    } else
      value = Double.parseDouble(arguments[VALUE_ARGUMENT]);
    log.trace(L_TR_PARSE);
    variables.put(var, value);
    log.trace(L_TR_VAR + var + L_TR_SET_COMPETE);
  }
}
