package Calculator.Test;

import Calculator.Context;
import Calculator.Exception.NotEnoughElementsInStackException;
import Calculator.Exception.WrongCountOfArgumentsException;
import Calculator.Operation.Subtraction;
import org.junit.*;

public class SubtractionTest {
  static final double delta = 0.000001;
  Context context = new Context();
  String[] s = {"-"};
  Subtraction sub = new Subtraction(context,s);
  @Test
  public void subtractionTest() {
    context.stack.push(25.0);
    context.stack.push(0.0);
    sub.execute();
    Assert.assertEquals(context.stack.pop(),-25 ,delta);
  }
  @Test
  public void subtractionTest1() {
    context.stack.push(25.0);
    context.stack.push(-12.5);
    sub.execute();
    Assert.assertEquals(context.stack.pop(),-37.5 ,delta);
  }
  @Test
  public void subtractionTest2() {
    context.stack.push(-25.0);
    context.stack.push(-5.0);
    sub.execute();
    Assert.assertEquals(context.stack.pop(),20 ,delta);
  }
  @Test
  public void subtractionTest3() {
    context.stack.push(12345678901234567890.0);
    context.stack.push(12345678901234567890.0);
    sub.execute();
    Assert.assertEquals(context.stack.pop(),0 ,delta);
  }
  @Test(expected = NotEnoughElementsInStackException.class)
  public void subtractionTest4() {
    context.stack.push(-0.009);
    sub.execute();
  }
  @Test(expected = WrongCountOfArgumentsException.class)
  public void subtractionTest5() {
    String[] s1 = {"-","223"};
    Subtraction sub1 = new Subtraction(context,s1);
  }
}