package Calculator.Test;
import Calculator.Context;
import Calculator.Exception.NotEnoughElementsInStackException;
import Calculator.Exception.WrongCountOfArgumentsException;
import Calculator.Operation.Print;
import static org.junit.Assert.*;

import Calculator.Operation.Print;
import org.junit.*;
public class PrintTest {
  static final double delta = 0.000001;
  Context context = new Context();
  String[] s = {"PRINT"};
  Print print = new Print(context,s);

  @Test
  public void printTest(){
    context.stack.push(432.1);
    context.stack.push(12.3);
    print.execute();
    assertEquals(context.stack.peekLast(),12.3,delta);
  }
  @Test(expected = NotEnoughElementsInStackException.class)
  public void printTest1(){
    context = new Context();
    print.execute();
  }
  @Test(expected = WrongCountOfArgumentsException.class)
  public void printTest2(){
    String[] s1 = {"PRINT","efsef"};
    Print print = new Print(context,s1);
  }

}