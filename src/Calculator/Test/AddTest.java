package Calculator.Test;

import Calculator.Context;
import Calculator.Exception.NotEnoughElementsInStackException;
import Calculator.Exception.WrongCountOfArgumentsException;
import Calculator.Operation.Add;
import org.junit.*;



public class AddTest {
  static final double delta = 0.000001;
  Context context = new Context();
  String[] s = {"+"};
  Add add = new Add(context,s);
  @Test
  public void testAdd() {
    context.stack.push(25.0);
    context.stack.push(0.0);
    add.execute();
    Assert.assertEquals(context.stack.pop(),25 ,delta);
  }
  @Test
  public void testAdd1() {
    context.stack.push(25.0);
    context.stack.push(-12.5);
    add.execute();
    Assert.assertEquals(context.stack.pop(),12.5 ,delta);
  }
  @Test
  public void testAdd2() {
    context.stack.push(-25.0);
    context.stack.push(-5.0);
    add.execute();
    Assert.assertEquals(context.stack.pop(),-30 ,delta);
  }
  @Test
  public void testAdd3() {
    context.stack.push(12345678901234567890.0);
    context.stack.push(-12345678901234567890.0);
    add.execute();
    Assert.assertEquals(context.stack.pop(),0 ,delta);
  }
  @Test(expected = NotEnoughElementsInStackException.class)
  public void testAdd4() {
    context.stack.push(-0.009);
    add.execute();
  }
  @Test(expected = WrongCountOfArgumentsException.class)
  public void testAdd5() {
    String[] s1 = {"+","223"};
    Add add1 = new Add(context,s1);
  }

}