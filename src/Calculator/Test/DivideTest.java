package Calculator.Test;

import Calculator.Context;
import Calculator.Exception.DivisionByZeroException;
import Calculator.Exception.NotEnoughElementsInStackException;
import Calculator.Exception.WrongCountOfArgumentsException;
import Calculator.Operation.Divide;
import org.junit.*;
import static org.junit.Assert.assertEquals;

public class DivideTest {
  static final double delta = 0.000001;
  Context context = new Context();
  String[] s = {"/"};
  Divide divide = new Divide(context,s);

  @Test
  public void divideTest(){
    context.stack.push(3.0);
    context.stack.push(1.0);
    divide.execute();
    assertEquals(context.stack.pop(),0.3333333,delta);
  }
  @Test
  public void divideTest1(){
    context.stack.push(1.0);
    context.stack.push(3.0);
    divide.execute();
    assertEquals(context.stack.pop(),3.0,delta);
  }
  @Test(expected = DivisionByZeroException.class)
  public void divideTest2(){
    context.stack.push(0.0);
    context.stack.push(1.0);
    divide.execute();
  }
  @Test(expected = NotEnoughElementsInStackException.class)
  public void divideTest3(){
    context.stack.push(1.0);
    divide.execute();
  }
  @Test(expected = WrongCountOfArgumentsException.class)
  public void divideTest4(){
    String[] s1 = {"/","2e"};
    Divide div1 = new Divide(context,s1);
  }

}