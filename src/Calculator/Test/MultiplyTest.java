package Calculator.Test;

import Calculator.Context;
import Calculator.Exception.NotEnoughElementsInStackException;
import Calculator.Exception.WrongCountOfArgumentsException;
import Calculator.Operation.Multiply;
import org.junit.*;
import static org.junit.Assert.assertEquals;

public class MultiplyTest {
  static final double delta = 0.000001;
  Context context = new Context();
  String[] s = {"*"};
  Multiply multiply = new Multiply(context, s);

  @Test
  public void multiplyTest() {
    context.stack.push(3.0);
    context.stack.push(0.333333333);
    multiply.execute();
    assertEquals(context.stack.pop(), 1.0, delta);
  }

  @Test
  public void multiplyTest1() {
    context.stack.push(0.0);
    context.stack.push(0.0);
    multiply.execute();
    assertEquals(context.stack.pop(), 0.0, delta);
  }

  @Test
  public void multiplyTest2() {
    context.stack.push(0.125);
    context.stack.push(32.0);
    multiply.execute();
    assertEquals(context.stack.pop(), 4.0, delta);
  }

  @Test(expected = NotEnoughElementsInStackException.class)
  public void multiplyTest4() {
    context.stack.push(1.0);
    multiply.execute();
  }

  @Test(expected = WrongCountOfArgumentsException.class)
  public void multiplyTest3() {
    String[] s1 = {"*", "2e"};
    Multiply div1 = new Multiply(context, s1);
  }

}