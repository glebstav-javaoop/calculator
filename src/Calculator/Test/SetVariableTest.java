package Calculator.Test;

import Calculator.Context;
import Calculator.Exception.WrongCountOfArgumentsException;
import Calculator.Operation.SetVariable;
import org.junit.Test;

import static org.junit.Assert.*;

public class SetVariableTest {
  static final double delta = 0.000001;
  Context context = new Context();
  @Test
  public void setVariableTest(){
    String[] s = {"DEFINE","b","23"};
    SetVariable variable = new SetVariable(context,s);
    variable.execute();
    assertEquals(context.variables.get("b"),23.0,delta);
  }
  @Test
  public void setVariableTest1(){
    String[] s = {"DEFINE","b","23"};
    SetVariable variable = new SetVariable(context,s);
    variable.execute();
    String[] s1 = {"DEFINE","b","65"};
    SetVariable variable1 = new SetVariable(context,s1);
    variable1.execute();
    assertEquals(context.variables.get("b"),65.0,delta);
  }
  @Test(expected = NumberFormatException.class)
  public void setVariableTest2(){
    String[] s = {"DEFINE","b","23.0bad"};
    SetVariable variable = new SetVariable(context,s);
    variable.execute();
  }
  @Test(expected = WrongCountOfArgumentsException.class)
  public void setVariableTest3(){
    String[] s = {"DEFINE","b"};
    SetVariable variable = new SetVariable(context,s);
  }

}