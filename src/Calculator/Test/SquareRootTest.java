package Calculator.Test;

import Calculator.Context;
import Calculator.Exception.GetComplexNumException;
import Calculator.Exception.NotEnoughElementsInStackException;
import Calculator.Operation.SquareRoot;
import org.junit.Test;

import static org.junit.Assert.*;

public class SquareRootTest {
  static final double delta = 0.000001;
  Context context = new Context();
  String[] s = {"SQRT"};
  SquareRoot sqrt = new SquareRoot(context,s);
  @Test
  public void squareRootTest(){
    context.stack.push(225.0);
    sqrt.execute();
    assertEquals(context.stack.pop(),15.0,delta);
  }
  @Test
  public void squareRootTest1(){
    context.stack.push(0.0);
    sqrt.execute();
    assertEquals(context.stack.pop(),0.0,delta);
  }
  @Test(expected = NotEnoughElementsInStackException.class)
  public void squareRootTest2(){
    sqrt.execute();
  }

  @Test(expected = GetComplexNumException.class)
  public void squareRootTest3(){
    context.stack.push(-16.0);
    sqrt.execute();
  }

}