package Calculator.Test;
import Calculator.Context;
import Calculator.Exception.NotEnoughElementsInStackException;
import Calculator.Exception.WrongCountOfArgumentsException;
import Calculator.Operation.Pop;
import static org.junit.Assert.*;
import org.junit.*;
public class PopTest {
  static final double delta = 0.000001;
  Context context = new Context();
  String[] s = {"POP"};
  Pop pop = new Pop(context,s);

  @Test
  public void popTest(){
    context.stack.push(432.1);
    context.stack.push(12.3);
    pop.execute();
    assertEquals(context.stack.pop(),432.1,delta);
  }
  @Test(expected = NotEnoughElementsInStackException.class)
  public void popTest1(){
    context.stack.push(432.1);
    context.stack.push(12.3);
    pop.execute();
    pop.execute();
    pop.execute();
  }
  @Test(expected = NotEnoughElementsInStackException.class)
  public void popTest2(){
    pop.execute();

  }
  @Test(expected = WrongCountOfArgumentsException.class)
  public void popTest3(){
    String[] s1 = {"POP","r5t6y"};
    Pop pop1 = new Pop(context,s1);
  }
}