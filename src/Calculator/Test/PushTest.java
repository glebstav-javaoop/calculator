package Calculator.Test;
import Calculator.Context;
import Calculator.Exception.WrongCountOfArgumentsException;
import Calculator.Operation.Push;
import static org.junit.Assert.*;

import Calculator.Operation.SetVariable;
import org.junit.*;
public class PushTest {
  static final double delta = 0.000001;
  @Test
  public void pushTest(){
    Context context = new Context();
    String[] s = {"PUSH","234"};
    Push push = new Push(context,s);
    push.execute();
    assertEquals(context.stack.pop(),234.0,delta);
  }
  @Test(expected = NumberFormatException.class)
  public void pushTest1(){
    Context context = new Context();
    String[] s = {"PUSH","acdsv234"};
    Push push = new Push(context,s);
    push.execute();
  }
  @Test
  public void pushTest2(){
    Context context = new Context();
    String[] s = {"PUSH","abc"};
    String[] var = {"DEFINE","abc","321.0"};
    SetVariable setVariable = new SetVariable(context,var);
    setVariable.execute();
    Push push = new Push(context,s);
    push.execute();
    assertEquals(context.stack.peekLast(),321.0,delta);
  }
  @Test(expected = WrongCountOfArgumentsException.class)
  public void pushTest4(){
    Context context = new Context();
    String[] s = {"PUSH","234","321"};
    Push push = new Push(context,s);
  }
}